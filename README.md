# kicad_ci_test

Test for Continuous Integration using KiCad.

This is an example of how to test and generate documentation for a KiCad project automatically every time you commit a change to the schematic and/or PCB.

In this example we use a [docker image](https://github.com/INTI-CMNB/kicad_auto_test) containing KiCad, [KiPlot](https://github.com/INTI-CMNB/kiplot) and all the supporting tools.

The [kicad_ci_test.kiplot.yaml](https://gitlab.com/set-soft/kicad-ci-test/-/blob/master/kicad_ci_test.kiplot.yaml) configures KiPlot and says what's available.

The [.gitlab-ci.yml](https://gitlab.com/set-soft/kicad-ci-test/-/blob/master/.gitlab-ci.yml) configures what GitLab makes.

In this file we:

* Instruct GitLab which stages are available:

```
stages:
  - run_erc
  - run_drc
  - gen_fab
```

* Then we define four actions and to which stage they belong. In our example *erc* belongs to *run_erc*, *drc* to *run_drc* and *sch_outputs* plus *pcb_outputs* belongs to *gen_fab*.
* Each action takes the repo content as input automatically
* The actions are executed by KiPlot and stored in the *Fabrication* directory
* The resulting files are stored as *artifacts* using:

```
  artifacts:
    paths:
      - Fabrication/
```

* We also define some dependencies to avoid running tasks for files that didn't change.

For more information about the GitLab configuration read the [GitLab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ee/ci/yaml/) page.

